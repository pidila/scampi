# Buttons

Présentation
------------

Le module buttons de Scampi reprend l'essentiel du code du module [buttons de Bootstrap 4](https://getbootstrap.com/docs/4.1/components/buttons/) en l'adaptant aux principes de Scampi : 
* une base pour l'usage courant ;
* des sous-modules optionnels pour ajouter des styles particuliers ;
* la définition des variables du module dans un fichier du module et non dans le fichier de settings généraux ;
* une attention particulière sur l'accessibilité.

Le module est composé d'un fichier maître (`_buttons.scss`) dans lequel sont définies toutes les variables et où l'on peut commenter ou décommenter l'import de styles complémentaires selon les besoins du projet. 


Utilisation
-----------

Le partial `_styles-buttons.scss` est indispensable tandis que les autres partials scss du module préfixés `_styles-buttons-xxxxxxx` sont optionnels en fonction des besoins du projet.

La présentation des boutons passe presque uniquement par la personnalisation des variables et l'ajout de classes sur les éléments html.

### Configuration

Les variables proposées dans ce module sont :

- `$input-btn-border-width` : bordure de l'input, sa valeur par défaut est `$border-width`;
- `$btn-border-width` : bordure du bouton, sa valeur par défaut est `$input-btn-border-width`;
- `$btn-padding-x` : padding horizontal, sa valeur par défaut est `1rem`;
- `$btn-padding-y` : padding vertical, sa valeur par défaut est `.25rem`;
- `$btn-default-color` : couleur du texte, sa valeur par défaut est `$gray-3`;
- `$btn-default-bg` : couleur du fond, sa valeur par défaut est `$gray-9`;
- `$btn-default-border` : couleur de la bordure, sa valeur par défaut est `darken($btn-default-bg, 10%)`;
- `$btn-primary-color` : couleur du bouton principal, sa valeur par défaut est `#fff`;
- `$btn-primary-bg` : couleur de fond du bouton principal, sa valeur par défaut est `$primary-color`;
- `$btn-primary-border` : couleur de la bordure du bouton principal, sa valeur par défaut est `darken($btn-primary-bg, 10%)`;
- `$btn-primary-outline` : couleur de l'outline du bouton principal, sa valeur par défaut est `$primary-color`;
- `$btn-secondary-color` : couleur du bouton secondaire, sa valeur par défaut est `$gray-3`;
- `$btn-secondary-bg` : couleur de fond du bouton secondaire, sa valeur par défaut est `#fff`;
- `$btn-secondary-border` : couleur de la bordure du bouton secondaire, sa valeur par défaut est `$gray-7`;
- `$btn-secondary-outline` : couleur de l'outline du bouton secondaire, sa valeur par défaut est `darken($btn-secondary-color, 30%)`;

De nombreuses autres variables personnalisables sont décrites dans le fichier [modules/buttons/_index.scss](https://gitlab.com/pidila/scampi/blob/master/modules/buttons/_index.scss).


Tout le socle commun des boutons est donné par la class `btn` ; d'autres classes viennent affiner la présentation ou ajouter des styles pour des besoins spécifiques. On trouve ainsi des partials pour :

<ul>
  <li><a href="buttons-block.html">buttons-block</a> : afficher des boutons sur toute la largeur disponible offerte par le parent ;</li>
  <li><a href="buttons-color.html">buttons-color</a> : faire varier la couleur du fond et/ou des bordures des boutons ;</li>
  <li><a href="buttons-group.html">buttons-group</a> : regrouper les boutons en "barres" ;</li>
  <li><a href="buttons-link.html">buttons-link</a> : donner l'aspect d'un lien à un bouton ;</li>
  <li><a href="buttons-size.html">buttons-size</a> : faire varier les tailles des boutons.</li>
</ul>

### Mixins disponibles

Le module met trois mixins à disposition, importés avec le module de base :

* button-variant : pour créer ou modifier les fonds, bordures et couleurs de texte ; gère aussi les états active, hover, focus ;
* button-outline-variant : comme ci-dessus mais pour des boutons outline ;
* button-size : pour faire varier la taille des boutons (texte, padding...)


Exemples d'utilisation
----------------------

````html
<h3>Bouton simple (uniquement la class btn)</h3>
<p><button type="button" class="btn" title="Envoyer">Envoyer</button></p>

<h3 >Bouton primaire (class btn-primary)</h3>
<p><button type="submit" class="btn btn-primary" title="Valider">Valider</button></p>

<h3>Bouton secondaire (class btn-secondary)</h3>
<p><button type="reset" class="btn btn-secondary" title="Annuler">Annuler</button></p>

<h3>Boutons désactivés (attribut disabled)</h3>
<button type="button" class="btn" disabled>Bouton simple</button>
<button type="button" class="btn btn-primary" disabled>Bouton primaire</button>
<button type="button" class="btn btn-secondary" disabled>Bouton secondaire</button>
````
