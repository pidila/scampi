# Skip-content

Présentation
-------------

Ce module permet de "passer" un contenu non accessible (comme une carte, un datepicker, une zone interactive, une image complexe, etc.) à l'aide de la touche de tabulation. 

### Accessibilité

Le contenu non accessible se trouve dans un container ayant l'attribut `aria-hidden="true"` pour que son contenu ne soit pas énoncé par les lecteurs d'écran.

Un texte, exploitable seulement par les synthèses vocales, explique qu'il existe un contenu qu'il est possible de l'éviter et pourquoi (par exemple car le contenu n'est pas pleinement accessible ou qu'il existe un piège clavier, etc.).

La tabulation au clavier fait apparaître les liens d'évitement avant et après ce contenu.

**Important :** cette solution ne doit être appliquée qu'en accompagnement d'une alternative accessible au contenu ainsi évitable.

Utilisation
-----------

Les styles présents dans le module sont les styles minimaux pour son bon fonctionnement.

Utiliser les touches <kbd>tab</kbd> et <kbd>shift + tab</kbd> pour faire apparaître les liens d’évitements de la démo.

### Configuration

Les variables utilisées dans ce module sont :
* `$skip-content-bg` : le fond du lien (par défaut gris très foncé) ;
* `$skip-content-color` : la couleur du texte des liens (par défaut gris très clair).

Exemple
-------

``` html
<div id="before-content">
  <p class="sr-only no-print" id="desc_skip_content">Ici le message indiquant qu'on peut passer le contenu et en expliquant les raisons.</p>
</div>

<p class="skip-content"><a class="skip-content-link" href="#after-content" aria-describedby="desc_skip_content">Passer le bloc suivant.</a></p>

<div aria-hidden="true">
  <p><i>Ici un contenu non compatible avec l’utilisation d’un lecteur d'écran (infographie, carte...). Il est masqué aux lecteurs d'écran et peut être "passé" en utilisant les touches <kbd>tab</kbd> ou <kbd>maj</kbd> + <kbd>tab</kbd> du clavier.</i></p>
</div>

<p class="skip-content"><a href="#before-content" aria-describedby="desc_skip_content" class="skip-content-link">Passer le bloc précédent.</a></p>

<p id="after-content">Bloc de contenu sur lequel le focus est repositionné si on a utilisé le lien d'évitement avant le contenu à éviter.</p>

```
