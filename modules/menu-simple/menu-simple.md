# Menu-simple

Présentation
------------

Permet d’afficher un menu simple (un seul niveau) à l’horizontale sur desktop et à la verticale sur mobile avec un bouton déclencheur d’ouverture/fermeture.

Si javascript n’est pas chargé dans le navigateur, sur mobile le bouton n’est pas affiché et le menu est affiché verticalement.


Utilisation
-----------

Les attributs nécessaires à la dynamique d’ouverture/fermeture et à son accessibilité sont injectés par le script.

Pour que le script fonctionne correctement, le code html doit répondre à deux impératifs :

1. L’élément déclencheur doit porter l’id `toggle-menu`, 
2. la liste constituant le menu doit porter la class `nav-main-list`.

En plus de ces attributs, on ajoutera la class `nav-item` sur chaque item de la liste.

Pour indiquer l’item actif du menu :
* désactiver le lien : transformer le lien en `span`
* indiquer visuellement l'état actif : ajouter la classe `.is-active`
* indiquer de manière non visuelle l'état actif : ajouter un `span` avec la classe `.sr-only` pour masquer visuellement son contenu et insérer l'indication qu'il s'agit de la page (ou rubrique) courante.


### Configuration

Les variables proposées dans ce module sont :

- `$enable-fontface` : activation du module, sa valeur par défaut est `false`

#### Desktop
- `$menu-simple-bg-color` : couleur de fond du menu, sa valeur par défaut est `$primary-color`
- `$menu-simple-text-color` : couleur du texte du menu, sa valeur par défaut est `#fff`
- `$menu-simple-bg-color-hover` : couleur de fond du menu au survol, sa valeur par défaut est `#000`
- `$menu-simple-text-color-hover` : couleur du texte du menu au survol, sa valeur par défaut est `#fff`
- `$menu-simple-bg-color-open` : couleur de fond du menu actif, sa valeur par défaut est `#000`
- `$menu-simple-text-color-open` : couleur du texte du menu actif, sa valeur par défaut est `#fff`

#### Mobile
- `$toggle-menu-bg-color` : couleur de fond du menu, sa valeur par défaut est `$primary-color`
- `$toggle-menu-text-color` : couleur du texte du menu, sa valeur par défaut est `#fff`
- `$toggle-menu-bg-color-hover` : couleur de fond du menu au survol, sa valeur par défaut est `#000`
- `$toggle-menu-text-color-hover` : couleur du texte du menu au survol, sa valeur par défaut est `#fff`
- `$toggle-menu-bg-color-open` : couleur de fond du menu ouvert, sa valeur par défaut est `$gray-2`
- `$toggle-menu-text-color-open` : couleur de fond du menu ouvert, sa valeur par défaut est `#fff`

### Accessibilité

Des attributs aria sur le bouton et le menu permettront aux utilisateurs pilotant une aide technique d’être informés de la nature de ces éléments.

### Script associé

Pour que ce module fonctionne, le script associé doit être appelé dans le pied de page, avant la fermeture du `body`.

Exemple
-------

```html
<button id="toggle-menu" class="toggle-menu"><span aria-hidden="true"></span>Menu</button>

<nav role="navigation" class="nav-main" id="nav-main" aria-label="menu principal">
  <div class="container">
    <ul class="nav-main-list">
      <li class="nav-item">
        <span class="nav-link is-active">
          <span class="sr-only">Page courante : </span>
          Accueil
        </span>
      </li>
      <li class="nav-item"><a class="nav-link" href="#visu">Identité visuelle</a></li>
      <li class="nav-item"><a class="nav-link" href="#typo">Contenus</a></li>
      <li class="nav-item"><a class="nav-link" href="#buttons">Boutons et formulaires</a></li>
      <li class="nav-item"><a class="nav-link" href="#composants">Composants du projet</a></li>
    </ul>
  </div>
 </nav>

```
