# Anchor focus

Présentation
-------------

Le script anchor-focus.js règle un bug présent dans Chrome/Safari/IE qui perturbe le déplacement du focus après un lien ancré.

Ces navigateurs ne transportent pas le focus sur la cible du lien d’évitement lorsqu’il a été activé, la tabulation suivante ira donc sur l'élément focusable suivant le lien d'origine au lieu d'aller à l'élément focusable suivant l'ancre de sa destination, ce qui pose de gros problèmes d'accessibilité.

Plus d’informations : [skip-navigation-link-not-working-in-google-chrome](http://stackoverflow.com/questions/3572843/skip-navigation-link-not-working-in-google-chrome/6188217#6188217)

Utilisation
-----------

Il n'y a rien d'autre à faire qu'insérer le sript dans les scripts du projet. À noter : [scampi-twig](../../scampi-twig) embarque ce script par défaut.
