# Collapse


Présentation
-------------------------------------------------------------------

Collapse est un composant html/js/css qui permet de plier et déplier des contenus. Une option permet d’indiquer les éventuels contenus dépliés par défaut.

Si javascript n’est pas chargé dans le navigateur, tous les contenus sont affichés.

Note : ce module utilise le script « _Accordéon_ » développé par [Switch](https://a11y.switch.paris/accordeon.html).


Utilisation
-------------------------------------------------------------------

Les attributs nécessaires à la dynamique d’ouverture/fermeture et à son accessibilité sont injectés par le script.

Pour que le script fonctionne correctement, le code html doit répondre à trois impératifs :

1. Les éléments à plier et déplier doivent se trouver dans un élément commun comportant la classe `js-expandmore`.
2. L’élément déclencheur (niveau de titre (hx), paragraphe (p) ou bloc (div)) doit porter la class `js-expandmore-title` et l’élément à montrer/cacher doit porter la class `js-expandmore-panel`.
3. Ces deux éléments doivent être voisins immédiats dans la source.

L'élément porteur de la classe `js-expandmore-title` ne contrôle que l'élément porteur de la classe `js-expandmore-panel` immédiatement voisin, pas les descendants de celui-ci.

Par ailleurs, il n'est pas possible d'imbriquer des éléments de classe `js-expandmore`. Par contre, un élément de classe `js-expandmore-panel` peut contenir des couples `js-expandmore-title` / `js-expandmore-panel` sans qu'il soit nécessaire de lui ajouter la classe `js-expandmore`. 

Par défaut un seul élément est dépliable à la fois. Pour permettre l'ouverture de tous les éléments en même temps il faut alors rajouter l'attribut `data-multiselectable="true"` sur l'élément ayant la classe `js-expandmore`.

Si un élément doit être affiché déplié par défaut, il faut ajouter l'attribut `data-expand="true"` sur son élément déclencheur.

Les styles présents dans le module scampi sont les styles minimaux pour son bon fonctionnement.

### Tout déplier

Il est possible d’ajouter un bouton pour déplier / replier tous les blocs de la page. Ce bouton doit porter la classe `js-expandmore-all`.

Le texte affiché dans ce bouton se configure dans les attributs `data-expandtext` pour le texte à afficher pour tout déplier et `data-collapsetext` pour celui à afficher pour tout replier.

Il est possible d'utiliser ce même bouton pour déplier / replier un seul bloc de la page en ajoutant l'attribut `data-controls` avec pour valeur l'`id` du block à controller. (Voir l'exemple ci-dessous.)

Il faut également ajouter l'attribut `data-expand="true"` au bouton porteur de la classe `js-expandmore-all` lorsqu'un ou plusieurs des éléments qu'il contrôle est affiché déplié par défaut.


### Script associé

Pour que ce module fonctionne, le script associé doit être appelé dans le pied de page, avant la fermeture du `body`.

Note : copier le script présent dans le module à l’endroit où sont rangés les autres scripts.

### Configuration

Les variables proposées dans ce module sont :

- `$border-color-collapse-focus` : couleur du symbole, sa valeur par défaut est `$link-color`;
- `$expandmore__button-padding` : padding du bouton, sa valeur par défaut est `0 1em`;
- `$expand-symbol-size` : taille du symbole, sa valeur par défaut est `.925em`;
- `$expand-symbol-closed` :  symbole fermé, sa valeur par défaut est `'+'`;
- `$expand-symbol-open` : symbole ouvert, sa valeur par défaut est `'−'`;
- `$expand-symbol-position-v` : position verticale du symbole, sa valeur par défaut est `top`;
- `$expand-symbol-position-v-value` : valeur de la position verticale, sa valeur par défaut est `.2em`;
- `$expand-symbol-position-h` : position horizontale du symbole, sa valeur par défaut est `left`;
- `$expand-symbol-position-h-value` : valeur de la position horizontale, sa valeur par défaut est `0`;
- `$expandmore__to_expand-padding` : padding de la zone dépliable, sa valeur par défaut est `0 1.5em`;


Exemple d'utilisation
-------------------------------------------------------------------

```html

<!-- Collapse simple -->
<div class="js-expandmore">
  <p class="js-expandmore-title">Un élément à déplier non compris dans le "tout déplier".</p>
  <p class="js-expandmore-panel">Montre également qu'on peut placer le futur bouton dans un autre élément qu'un niveau de titre (ici p, mais div fonctionne aussi).
</div>

<!-- Regroupement de blocs à déplier -->
<button class="js-expandmore-all btn" data-expand="true" data-expandtext="Tout déplier" data-collapsetext="Tout replier" data-controls="questions">Tout déplier</button>

<div id="questions" class="js-expandmore" data-multiselectable="true">
  <h3 class="js-expandmore-title">Question 1</h3>
  <div class="js-expandmore-panel">
    <p>Le contenu caché peut être constitué de tout élément html : paragraphes, titres, tableaux, images…</p>
  </div>

  <h3 class="js-expandmore-title">Question 2</h3>
  <div class="js-expandmore-panel">
    <p>Le contenu caché peut être constitué de tout élément html : paragraphes, titres, tableaux, images…</p>
  </div>

  <h3 class="js-expandmore-title" data-expand>Faut-il une autre question sur plusieurs lignes pour vérifier le comportement du symbole d'ouverture et de fermeture ?</h3>
  <div class="js-expandmore-panel is-opened">
    <p>Oui, il le faut</p>
    <p>Ce collapse est ouvert par défaut et peut être replié.</p>
  </div>

</div>

<br>

<div class="js-expandmore">
  <p class="js-expandmore-title">Un élément à déplier non compris dans le "tout déplier".</p>
  <p class="js-expandmore-panel">Montre également qu'on peut placer le futur bouton dans un autre élément qu'un niveau de titre (ici p, mais div fonctionne aussi).
</div>
```
