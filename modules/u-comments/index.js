/* sg-comments.js
    Affiche/Masque tous les éléments portant la class="sg-comments"

    Le bouton déclencheur doit être posé dans la source :

    <button id="sg-toggle-comments" class="btn sg-toggle-comments is-closed" arial-expanded="false">commentaires</button>

    (le texte afficher/masquer est introduit via css)
*/
var Scampi = Scampi || {};

Scampi.uComments = function uComments(){
  var toggle = document.getElementById('sg-toggle-comments');
  var comments = document.querySelectorAll('.sg-comment');

  if(!toggle){
    return;
  }

  function toggleVisibility(evt){
    Array.prototype.forEach.call(comments, function(comment){
      comment.classList.toggle('is-visible');
      toggle.classList.toggle('is-closed');

      var isExpanded = toggle.getAttribute('aria-expanded') === 'true';
      toggle.setAttribute('aria-expanded', isExpanded ? 'false' : 'true');
    });
  }

  toggle.addEventListener('click', toggleVisibility);
}

Scampi.uComments();
