# Blockquote


Présentation
-------------

Les blocs de citation sont composés de la citation elle-même et de la source de la citation.

Note : ce module est basé sur celui du framework Bootstrap.

Utilisation
-------------

L’élément `blockquote` est utilisé pour baliser (et éventuellement mettre en forme) une citation. Il est composé d'un `<p>` pour la citation et d'un `<footer>` pour la source.
Les styles par défaut sont définis dans le mixin blockquote.


```` scss
blockquote {
  @include blockquote();
}
````

### Configuration

Les variables proposées dans ce module sont :
- `$blockquote-small-color` : couleur du texte de la source, sa valeur par défaut est `$gray`;
- `$blockquote-border-color` : Couleur de la bordure, sa valeur par défaut est `$gray-8`;


Exemple d'utilisation
-------------

```` html
<blockquote>
  <p>Mettre le Web et ses services à la disposition de tous les individus, quels que soient leur matériel ou logiciel, leur infrastructure réseau, leur langue maternelle, leur culture, leur localisation géographique, ou leurs aptitudes physiques ou mentales.</p>
  <footer>Tim Berners-Lee, inventeur du Web.</footer>
</blockquote>
````
